"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logSetDebug = (setName, set) => {
    if (process.env.JSON_SCHEMA_DIFF_ENABLE_DEBUG === 'true') {
        console.log(`\n${setName}`);
        console.log(JSON.stringify(set.toJsonSchema(), null, 2));
    }
};
