"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const subset_1 = require("../set/subset/subset");
const type_set_1 = require("../set/type-set");
const is_type_supported_1 = require("./is-type-supported");
const createTypeSubset = (setType, types) => is_type_supported_1.isTypeSupported(types, setType) ? new subset_1.AllSubset(setType) : new subset_1.EmptySubset(setType);
exports.createTypeSet = (setType, types) => type_set_1.createTypeSetFromSubsets(setType, [createTypeSubset(setType, types)]);
