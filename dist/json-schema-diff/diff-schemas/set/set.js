"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addAnyOfIfNecessary = (schemas) => schemas.length === 1 ? schemas[0] : { anyOf: schemas };
